#include <stdlib.h>
#include <stdio.h>
#include "check_sudoku.h"

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("Usage: ./example SUDOKU_FILE\n");
        printf("  SUDOKU_FILE: Text file representing a sudoku board with 9x9 digits.\n");
        return EXIT_FAILURE;
    }

    const char *filename = argv[1];
    sudoku_board *p_board = read_board_from_file(filename);
    if (p_board == NULL) {
        return EXIT_FAILURE;
    }

    printf("Loaded board:\n");
    print_board(p_board, false);

    bool is_solution = check_if_board_is_solution(p_board);

    if (is_solution) {
        printf("Correct sudoku solution!\n");
    } else {
        printf("Not a correct sudoku solution!\n");
    }

    free(p_board);
    return EXIT_SUCCESS;
}
