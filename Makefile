CFLAGS = -Wall -Werror -Wpedantic -Wunused-result

PROGRAMS = example

example: check_sudoku.o example.o
	gcc $(CFLAGS) $^ -o $@

%.o: %.c
	gcc $(CFLAGS) -c $< -o $@

clean:
	rm -f *.o $(PROGRAMS)
