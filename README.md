# Sudoku Checker

Multithread project to check whether a sudoku board is a valid solution.

#### Example usage

    ./example SUDOKU_FILE
