#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <pthread.h>
#include "check_sudoku.h"

#define BOARD_SIZE 9

typedef uint8_t value_t;

/* Sudoku board with integers matrix. */
struct sudoku_board {
    // Values matrix.
    value_t matrix[BOARD_SIZE][BOARD_SIZE];
};

/* Arguments to be passed to check functions. */
typedef struct {
    // Pointer to sudoku board.
    const sudoku_board *p_board;
    // Result to be returned from thread.
    bool result;
} check_args;

/* Fill array with desired board row values. */
static void get_row_as_array(const sudoku_board *p_board, size_t row, value_t array[BOARD_SIZE]);

/* Fill array with desired board column values. */
static void get_col_as_array(const sudoku_board *p_board, size_t col, value_t array[BOARD_SIZE]);

/* Fill array with desired 3x3 board square values. */
static void get_square_as_array(const sudoku_board *p_board, size_t big_row, size_t big_col, value_t array[BOARD_SIZE]);

/* Checks if elements has all 9 different digits. */
static bool check_valid_elements(const value_t elements[BOARD_SIZE]);

/* Check elements of each board row. */
static void *check_rows(void *arguments);

/* Check elements of each board column. */
static void *check_cols(void *arguments);

/* Check elements of each board square. */
static void *check_squares(void *arguments);

/* Transform value to char. */
static inline char to_char(value_t value);

/* * * * * * *
 * FUNCTIONS *
 * * * * * * */

sudoku_board *read_board_from_file(const char *filename) {
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        printf("Error: no file named %s\n", filename);
        return NULL;
    }

    sudoku_board *p_board = malloc(sizeof(sudoku_board));
    if (p_board == NULL) {
        printf("Error: failed to allocate memory for board\n");
        return NULL;
    }

    for (size_t i = 0; i < BOARD_SIZE; i++) {
        for (size_t j = 0; j < BOARD_SIZE; j++) {
            int ret = fscanf(f, "%" SCNu8, &p_board->matrix[i][j]);
            if (ret < 1) {
                printf("Error: failed to read values from sudoku board\n");
                free(p_board);
                return NULL;
            }
        }
    }

    return p_board;
}

void print_board(const sudoku_board *p_board, bool simple_print) {
    if (simple_print) {
        for (size_t i = 0; i < BOARD_SIZE; i++) {
            for (size_t j = 0; j < BOARD_SIZE; j++) {
                printf("%" PRIu8 " ", p_board->matrix[i][j]);
            }

            printf("\n");
        }
    } else {
        for (size_t i = 0; i < BOARD_SIZE; i++) {
            printf("%c %c %c | %c %c %c | %c %c %c \n",
                to_char(p_board->matrix[i][0]),
                to_char(p_board->matrix[i][1]),
                to_char(p_board->matrix[i][2]),
                to_char(p_board->matrix[i][3]),
                to_char(p_board->matrix[i][4]),
                to_char(p_board->matrix[i][5]),
                to_char(p_board->matrix[i][6]),
                to_char(p_board->matrix[i][7]),
                to_char(p_board->matrix[i][8])
            );

            if (i == 2 || i == 5) {
                printf("- - - + - - - + - - - \n");
            }
        }
    }
}

bool check_if_board_is_solution(const sudoku_board *p_board) {
    pthread_t threads[3];

    // Check rows.
    check_args row_args;
    row_args.p_board = p_board;
    pthread_create(&threads[0], NULL, check_rows, &row_args);

    // Check columns.
    check_args col_args;
    col_args.p_board = p_board;
    pthread_create(&threads[1], NULL, check_cols, &col_args);

    // Check 3x3 squares.
    check_args square_args;
    square_args.p_board = p_board;
    pthread_create(&threads[2], NULL, check_squares, &square_args);

    // Join threads.
    for (size_t i = 0; i < 3; i++) {
        pthread_join(threads[i], NULL);
    }

    bool is_solution = row_args.result && col_args.result && square_args.result;
    return is_solution;
}

/* * * * * * * * * * *
 * STATIC FUNCTIONS  *
 * * * * * * * * * * */

void get_row_as_array(const sudoku_board *p_board, size_t row, value_t array[BOARD_SIZE]) {
    for (size_t j = 0; j < BOARD_SIZE; j++) {
        array[j] = p_board->matrix[row][j];
    }
}

void get_col_as_array(const sudoku_board *p_board, size_t col, value_t array[BOARD_SIZE]) {
    for (size_t i = 0; i < BOARD_SIZE; i++) {
        array[i] = p_board->matrix[i][col];
    }
}

void get_square_as_array(const sudoku_board *p_board, size_t big_row, size_t big_col, value_t array[BOARD_SIZE]) {
    size_t first_row = 3 * big_row;
    size_t first_col = 3 * big_col;

    size_t array_pos = 0;
    for (size_t i = first_row; i < first_row + 3; i++) {
        for (size_t j = first_col; j < first_col + 3; j++) {
            array[array_pos] = p_board->matrix[i][j];
            array_pos++;
        }
    }
}

bool check_valid_elements(const value_t elements[BOARD_SIZE]) {
    // Initialize array to keep track of elements.
    // Position i store info about value (i + 1).
    bool has_value[BOARD_SIZE];
    for (size_t i = 0; i < BOARD_SIZE; i++) {
        has_value[i] = false;
    }

    bool return_value = true;
    for (size_t i = 0; i < BOARD_SIZE; i++) {
        if (1 <= elements[i] && elements[i] <= BOARD_SIZE) {
            if (!has_value[elements[i] - 1]) {
                has_value[elements[i] - 1] = true;
            } else {
                return_value = false;
            }

        } else {
            printf("Error: invalid sudoku digit: %d\n", elements[i]);
            return_value = false;
        }
    }

    return return_value;
}

void *check_rows(void *arguments) {
    check_args *args = (check_args *) arguments;
    bool valid = true;
    for (size_t row = 0; row < BOARD_SIZE; row++) {
        value_t array[BOARD_SIZE];
        get_row_as_array(args->p_board, row, array);
        if (!check_valid_elements(array)) {
            printf("Mistake at row %zu\n", row);
            valid = false;
        }
    }

    args->result = valid;
    return NULL;
}

void *check_cols(void *arguments) {
    check_args *args = (check_args *) arguments;
    bool valid = true;
    for (size_t col = 0; col < BOARD_SIZE; col++) {
        value_t array[BOARD_SIZE];
        get_col_as_array(args->p_board, col, array);
        if (!check_valid_elements(array)) {
            printf("Mistake at column %zu\n", col);
            valid = false;
        }
    }

    args->result = valid;
    return NULL;
}

void *check_squares(void *arguments) {
    check_args *args = (check_args *) arguments;
    bool valid = true;
    for (size_t big_row = 0; big_row < 3; big_row++) {
        for (size_t big_col = 0; big_col < 3; big_col++) {
            value_t array[BOARD_SIZE];
            get_square_as_array(args->p_board, big_row, big_col, array);
            if (!check_valid_elements(array)) {
                printf("Mistake at big square %zux%zu\n", big_row, big_col);
                valid = false;
            }
        }
    }

    args->result = valid;
    return NULL;
}

static inline char to_char(value_t value) {
    return value + '0';
}
