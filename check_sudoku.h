#ifndef __CHECK_SUDOKU_H__
#define __CHECK_SUDOKU_H__

#include <stdbool.h>

/* Sudoku board with integers matrix. */
typedef struct sudoku_board sudoku_board;

/**
 * Read a sudoku board from a text file.
 * After used, the pointer must be freed.
 *
 * @param filename Text file name.
 * @return Sudoku board with file data.
 */
sudoku_board *read_board_from_file(const char *filename);

/**
 * Print the sudoku board into the terminal.
 *
 * @param board Sudoku board.
 * @param simple_print False for a prettier print and true for no formatting.
 */
void print_board(const sudoku_board *board, bool simple_print);

/**
 * Check if a board is a valid sudoku solution.
 *
 * @param board Sudoku board.
 * @return A boolean indicating if the solution is valid.
 */
bool check_if_board_is_solution(const sudoku_board *board);

#endif // __CHECK_SUDOKU_H__
